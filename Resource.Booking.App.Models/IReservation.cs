﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Resource.Booking.App.Models
{
 public interface IReservation
 {
  int Room { get; set; }
  string Guest { get; set; }
  DateTime Date { get; set; }
 }
}
