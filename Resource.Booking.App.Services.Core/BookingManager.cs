﻿using Resource.Booking.App.Services;
using Resource.Booking.App.Models;
using Resource.Booking.App.Models.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Resource.Booking.App.Services.Core
{
 public class BookingManager : IBookingManager
 {

  IReservationService _reservationService;

  public BookingManager(IReservationService reservationService)
  {
   _reservationService = reservationService;
  }

  public void AddBooking(string guest, int room, DateTime date)
  {

    StringBuilder validations = new StringBuilder();
    bool hasInvalidArgs = false;
    if (String.IsNullOrWhiteSpace(guest))
    {
     hasInvalidArgs = true;
     validations.AppendLine("Invalid value for argument [guest]. Cannot be null or empty.");
    }
    if (room < 1)
    {
     hasInvalidArgs = true;
     validations.AppendLine("Invalid value for argument [room]. Cannot be less than 1");
    }
    if (DateTime.Compare(date, DateTime.Now.Date) < 0)
    {
     hasInvalidArgs = true;
     validations.AppendLine("Invalid value for argument [date]. Cannot be past date.");
    }
   if (!hasInvalidArgs)
    _reservationService.MakeReservation(new Reservation(guest, room, date));
   else
   {
    throw new ArgumentException(validations.ToString());
   }
  }

  public bool IsRoomAvailable(int room, DateTime date)
  {
   return _reservationService.IsRoomAvailable(room, date);
  }
  public IEnumerable<int> GetAvailableRooms(DateTime date)
  {
   return _reservationService.GetAvailableRooms(date);
  }
 }
}
