﻿using System;
using System.Collections.Generic;
using System.Text;
using Resource.Booking.App.Models;
using System.Linq;

namespace Resource.Booking.App.Services.Core
{
 public class ReservationService : IReservationService
 {
  int[] _rooms;
  List<IReservation> _reservations;
  private static ReservationService _instance = null;
  private static readonly object _threadlock = new object();
  private ReservationService()
  {
   _reservations = new List<IReservation>();
   _rooms = new int[] { 101, 102, 201, 203 };
  }

  public static IReservationService GetSingletonInstance()
  {
   if (_instance == null)
   {
    lock (_threadlock)
    {
     if (_instance == null)
     {
      _instance = new ReservationService();
     }
    }
   }
   return _instance;
  }
  public void CancelReservation(IReservation reservation)
  {
   lock (_threadlock)
   {
    if (_reservations.Contains(reservation))
     _reservations.Remove(reservation);
   }
  }

  public IEnumerable<IReservation> GetReservations()
  {
   return _reservations;
  }

  public bool MakeReservation(IReservation reservation)
  {
   bool retval = false;
   lock (_threadlock)
   {
    if (IsRoomAvailable(reservation.Room, reservation.Date))
    {
     _reservations.Add(reservation);
     retval = true;
    }
   }
   return retval;
  }

  public bool IsRoomAvailable(int room, DateTime date)
  {
   bool retval = false;
   if (!_reservations.Any(x => room == x.Room && DateTime.Compare(date, x.Date) == 0))
   {
    retval = true;
   }
   return retval;
  }

  public IEnumerable<int> GetAvailableRooms(DateTime date)
  {
   return _rooms.Except(_reservations.Where(x => DateTime.Compare(x.Date, date) == 0).Select(x => x.Room).ToArray()).ToList();
  }
 }
}
