﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resource.Booking.App.Services.Core;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Resource.Booking.App.Services.Core.Tests
{
 [TestClass()]
 public class BookingManagerTests
 {

  IBookingManager _bookingManager;
  public BookingManagerTests()
  {
   RegisterServices();
  }

  void RegisterServices()
  {
   // IOC and Dependency Injection using Microsoft.Extensions.DependencyInjection
   var serviceCollection = new ServiceCollection();
   serviceCollection.AddScoped<IBookingManager, BookingManager>();
   IReservationService reservationService = ReservationService.GetSingletonInstance();
   serviceCollection.AddSingleton<IReservationService>(reservationService); ;

   IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
   _bookingManager = serviceProvider.GetService<IBookingManager>();

   if (serviceProvider != null && serviceProvider is IDisposable)
   {
    ((IDisposable)serviceProvider).Dispose();
   }

  }

  [TestMethod()]
  public void AddBookingTest_BookSingle()
  {
   var today = new DateTime(2012, 3, 8);
   var otherDay = new DateTime(2012, 4, 8);
   _bookingManager.AddBooking("test", 101, today);
   Assert.IsTrue(!_bookingManager.IsRoomAvailable(101, today) && _bookingManager.IsRoomAvailable(101, otherDay));
  }

  [TestMethod()]
  public void AddBookingTest_BookMultiple()
  {
   var today = new DateTime(2012, 1, 8);
   var otherDay = new DateTime(2012, 2, 8);
   _bookingManager.AddBooking("test", 101, today);
   _bookingManager.AddBooking("test", 102, today);
   Assert.IsTrue(!_bookingManager.IsRoomAvailable(101, today) 
    && !_bookingManager.IsRoomAvailable(102, today)
    && _bookingManager.IsRoomAvailable(101, otherDay)
    && _bookingManager.IsRoomAvailable(102, otherDay)
    );
  }

  [TestMethod()]
  public void GetAvailableRooms_TestSome()
  {
   var today = new DateTime(2012, 6, 8);
   var otherDay = new DateTime(2012, 5, 8);
   _bookingManager.AddBooking("test", 101, today);
   _bookingManager.AddBooking("test", 102, today);
   _bookingManager.AddBooking("test", 201, otherDay);
   IEnumerable<int> resultsToday = _bookingManager.GetAvailableRooms(today);
   IEnumerable<int> resultsOtherDay = _bookingManager.GetAvailableRooms(otherDay);
   Assert.IsTrue(resultsToday.ToArray().Length == 2 && resultsOtherDay.ToArray().Length == 3);
  }

  [TestMethod()]
  public void GetAvailableRooms_TestNone()
  {
   var today = new DateTime(2012, 3, 8);
   _bookingManager.AddBooking("test", 101, today);
   _bookingManager.AddBooking("test", 102, today);
   _bookingManager.AddBooking("test", 201, today);
   _bookingManager.AddBooking("test", 203, today);
   IEnumerable<int> results = _bookingManager.GetAvailableRooms(today);
   Assert.IsTrue(results.ToArray().Length == 0);

  }




 }
 }