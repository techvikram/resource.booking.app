﻿using Resource.Booking.App.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Resource.Booking.App.Services
{
 public interface IReservationService
 {
  bool MakeReservation(IReservation reservation);
  bool IsRoomAvailable(int room, DateTime date);
  IEnumerable<int> GetAvailableRooms(DateTime date);
  void CancelReservation(IReservation reservation);
  IEnumerable<IReservation> GetReservations();
 }
}
