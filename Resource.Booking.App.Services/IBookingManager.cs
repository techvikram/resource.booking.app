﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Resource.Booking.App.Services
{
 public interface IBookingManager
 {
  bool IsRoomAvailable(int room, DateTime date);

  void AddBooking(string guest, int room, DateTime date);

  public IEnumerable<int> GetAvailableRooms(DateTime date);
 }
}
