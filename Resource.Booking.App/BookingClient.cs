﻿using Resource.Booking.App.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Resource.Booking.App
{
 public class BookingClient
 {

  IBookingManager _bookingManager;

  /// <summary>
  /// Runtime Constructor injection of concrete types
  /// </summary>
  public BookingClient(IBookingManager bookingManager)
  {
   _bookingManager = bookingManager;
  }

  public void Run()
  {
   try
   {
    var today = new DateTime(2012, 3, 8);
    Console.WriteLine(String.Join(",", _bookingManager.GetAvailableRooms(today)));
    Console.WriteLine(_bookingManager.IsRoomAvailable(101, today));
    _bookingManager.AddBooking("test", 101, today);
    Console.WriteLine(_bookingManager.IsRoomAvailable(101, today));
    Console.WriteLine(String.Join(",", _bookingManager.GetAvailableRooms(today)));
    Console.ReadLine();
   } 
   catch(Exception ex)
   {
    // handle exception.
   }
  }

 }
}
