﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Resource.Booking.App.Services;
using Resource.Booking.App.Services.Core;

namespace Resource.Booking.App
{
 class Program
 {
  static void Main(string[] args)
  {

   // IOC and Dependency Injection using Microsoft.Extensions.DependencyInjection
   // Register Service implentations
   var serviceCollection = new ServiceCollection();
   ConfigureServices(serviceCollection);

   IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
   IServiceScope scope = serviceProvider.CreateScope();
   scope.ServiceProvider.GetRequiredService<BookingClient>().Run();
   if (serviceProvider != null && serviceProvider is IDisposable)
   {
    ((IDisposable)serviceProvider).Dispose();
   }

  }

  private static void ConfigureServices(IServiceCollection serviceCollection)
  {
   serviceCollection.AddScoped<IBookingManager, BookingManager>();
   IReservationService reservationService = ReservationService.GetSingletonInstance();
   serviceCollection.AddSingleton<IReservationService>(reservationService);

   serviceCollection.AddSingleton<BookingClient>();

  }
 }
}
