# BookingManager Implementation

- Core logic is implmented in **Resource.Booking.App.Services.Core.ReservationService** which implements Singleton Design pattern with Thread Safe checks 

- **Resource.Booking.App.Services.Core.BookingManager** uses Resource.Booking.App.Services.Core.ReservationService

- Uses Depedency Injection to resolve Service dependencies