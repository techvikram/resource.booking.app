﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Resource.Booking.App.Models.Core
{
 public class Reservation : IReservation
 {

  public Reservation()
  {

  }

  public Reservation(string guest, int room, DateTime date)
  {
   Guest = guest;
   Room = room;
   Date = date;
  }
  public int Room { get; set; }
  public string Guest { get; set; }
  public DateTime Date { get; set; }
 }
}
